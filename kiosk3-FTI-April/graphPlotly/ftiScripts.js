var repeatID;

// Repeatedly call the loadPlotDataFunc()
function repeatLoadPlotData() {
 // repeatID = setInterval(loadPlotDataFunc, 1000);
 loadPlotDataFunc()
};

function loadPlotDataFunc() {
    //load the data from a file
        //console.log("updating plot")
        var cdData12 = (function() {
        var cdData12 = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "cdPlotly1.json",
            'dataType': "json",
            'success': function(data) {
                cdData12 = data;
            }
        });
        return cdData12;
        })();  
    
    //console.log(cdData12)
    // plot the data with Plotly
    var data = [ cdData12["cdDataTrail2"], cdData12["cdDataTrail4"]];

    var layout = {};
    var config = {
        toImageButtonOptions: {
          format: 'svg', // one of png, svg, jpeg, webp
          filename: 'custom_image', 
          height: 900,
          width: 1200,
          scale: 6 // Multiply title/legend/axis/canvas sizes by this factor
        }
      };
    // plot the data
    Plotly.newPlot('myDiv', data, layout, config , {showSendToCloud: true });
    
}; // End of loadPlotDataFunc()

//send REST POST message
function sendRESTfti() {
    var postData = {"runFTI":1}
    $.ajax({
        url: "http://10.4.17.222:8080/fti",
        contentType: "application/json",
        dataType: "json",
        type: "POST", //send it through get method
        data: postData
        })
    };

// Repeatedly call the updateStats()
function repeatUpdateStats(nodeName) {
  var nodeName = nodeName;
  repeatID = setInterval(function(){updateStats(nodeName);}, 2000); //need anonymous function here to pass parameter
};

//reload stats for all nodes
function updateStats(nodeName) {
    var nodeName = nodeName;
    console.log("update" + nodeName)
    // load stats for all nodes
    var statsAll = (function() {
    var statsAll = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': "statsAll.json",
        'dataType': "json",
        'success': function(data) {
            statsAll = data;
        }
    });
    return statsAll;
    })();  
    
    //upate the alpha table
    //trails to be updated
    if(nodeName == "alpha"){
        trails = ["Trail 1", "Trail 2", "Trail 3", "Trail 4"]
        updateTable("alpha", statsAll, trails)
        
    };
        
    //upate the charlie table
    //trails to be updated
    if(nodeName == "charlie"){
        trails = ["Trail 3", "Trail 5", "Trail 9", "Trail 10"]
        updateTable("charlie", statsAll, trails)
    };
    
    //upate the delta table
    //trails to be updated
    if(nodeName == 'delta'){
        trails = ["Trail 4", "Trail 5", "Trail 9", "Trail 10"]
        updateTable("delta", statsAll, trails)
    };
        
    //upate the saturn table
    //trails to be updated
    if(nodeName == 'saturn'){
        trails = ["Trail 1", "Trail 2"]
        updateTable("saturn", statsAll, trails)
    }
        
}; //end of updateStats()

function updateTable(nodeName, statsAll, trails){
    // update table 
    var statsNode = statsAll[nodeName];
    console.log(statsNode)
    
    var numTrails = trails.length;
    var ii = 0; //counter for trails
    var jj = 0; //counter for values in trail
    var row = []; //array of values in each row
    var obj = []; //dictionary object of values in a trail
    var newData = []; //2D array of new table values
    
    for (ii = 0; ii < numTrails; ii++){
        //step through each trail
        row = [];
        jj = 0;
        obj = statsNode[trails[ii]];
        //console.log(obj);
        for (const key in obj) {
            // step through each value in the trail
            let value = obj[key];
            row[jj] = value;
            //update the table
            rowCol = (jj.toString()).concat(ii.toString()).concat(nodeName);
            var br = document.getElementById(rowCol);
            br.textContent = value;
            jj = jj + 1;
        }
        //add row to newData table
        newData.push(row);
        //console.log(row);
    }
    
    //For reference, this is the array of new values
    //console.log(newData);  
    
 };   //end of update Table