$(document).ready(function() {
  var state = 0;

	// On left press
  $("#left-click").mousedown(function() {
		// Advance state
    state = (state-1+9)%9;

    if (state == 0) {
      $("#lottie_00").show();
      $("#liveIframe").hide();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 1) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").show();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 2) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").show();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 3) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").show();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 4) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").show();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 5) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").show();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 6) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").show();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 7) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").show();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 8) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").show();
      $("#lottie_09").hide();
    } else if (state == 9) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").show();
    }
  });

	// On right press
  $("#right-click").mousedown(function() {
		// Advance state
    state = (state+1)%9;

    if (state == 0) {
      $("#lottie_00").show();
      $("#liveIframe").hide();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 1) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").show();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 2) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").show();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 3) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").show();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 4) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").show();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 5) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").show();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 6) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").show();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 7) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").show();
      $("#lottie_08").hide();
      $("#lottie_09").hide();
    } else if (state == 8) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").show();
      $("#lottie_09").hide();
    } else if (state == 9) {
      $("#lottie_00").hide();
      $("#liveIframe").show();
      $("#lottie_01").hide();
      $("#lottie_02").hide();
      $("#lottie_03").hide();
      $("#lottie_04").hide();
      $("#lottie_05").hide();
      $("#lottie_06").hide();
      $("#lottie_07").hide();
      $("#lottie_08").hide();
      $("#lottie_09").show();
    }
  });
});
